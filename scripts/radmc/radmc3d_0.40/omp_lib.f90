module omp_lib
!
! OpenMP Fortran API v2.5
!       
        implicit none
        
        integer, parameter :: sgl = kind( 0.0 )
        integer, parameter :: dbl = kind( 0.0d0 )
        
        integer, parameter :: omp_real_kind = dbl
        integer, parameter :: omp_integer_kind = sgl
        integer, parameter :: omp_logical_kind = sgl
        integer, parameter :: omp_lock_kind = dbl
        integer, parameter :: omp_nest_lock_kind = dbl
        
        interface
                subroutine omp_destroy_lock ( var )
                        import :: omp_lock_kind
                        integer ( kind=omp_lock_kind ), intent(inout) :: var
                end subroutine omp_destroy_lock

                subroutine omp_destroy_nest_lock ( var )
                        import :: omp_nest_lock_kind
                        integer ( kind=omp_nest_lock_kind ), intent(inout) :: var
                end subroutine omp_destroy_nest_lock

                function omp_get_dynamic ()
                        import :: omp_logical_kind
                        logical ( kind=omp_logical_kind ) :: omp_get_dynamic
                end function omp_get_dynamic

                function omp_get_max_threads ()
                        import :: omp_integer_kind
                        integer ( kind=omp_integer_kind ) :: omp_get_max_threads
                end function omp_get_max_threads

                function omp_get_nested ()
                        import :: omp_logical_kind
                        logical ( kind=omp_logical_kind ) :: omp_get_nested
                end function omp_get_nested

                function omp_get_num_procs ()
                        import :: omp_integer_kind
                        integer ( kind=omp_integer_kind ) :: omp_get_num_procs
                end function omp_get_num_procs

                function omp_get_num_threads ()
                        import :: omp_integer_kind
                        integer ( kind=omp_integer_kind ) :: omp_get_num_threads
                end function omp_get_num_threads

                function omp_get_thread_num ()
                        import :: omp_integer_kind
                        integer ( kind=omp_integer_kind ) :: omp_get_thread_num
                end function omp_get_thread_num

                function omp_get_wtick ()
                        import :: omp_real_kind
                        real ( kind=omp_real_kind ) :: omp_get_wtick
                end function omp_get_wtick

                function omp_get_wtime ()
                        import :: omp_real_kind
                        real ( kind=omp_real_kind ) :: omp_get_wtime
                end function omp_get_wtime

                subroutine omp_init_lock ( var )
                        import :: omp_lock_kind
                        integer ( kind=omp_lock_kind ), intent(out) :: var
                end subroutine omp_init_lock

                subroutine omp_init_nest_lock ( var )
                        import :: omp_nest_lock_kind
                        integer ( kind=omp_nest_lock_kind ), intent(out) :: var
                end subroutine omp_init_nest_lock

                function omp_in_parallel ()
                        import :: omp_logical_kind
                        logical ( kind=omp_logical_kind ) :: omp_in_parallel
                end function omp_in_parallel

                subroutine omp_set_dynamic ( enable_expr )
                        import :: omp_logical_kind
                        logical ( kind=omp_logical_kind ), intent(in) :: enable_expr
                end subroutine omp_set_dynamic

                subroutine omp_set_lock ( var )
                        import :: omp_lock_kind
                        integer ( kind=omp_lock_kind ), intent(inout) :: var
                end subroutine omp_set_lock

                subroutine omp_set_nest_lock ( var )
                        import :: omp_nest_lock_kind
                        integer ( kind=omp_nest_lock_kind ), intent(inout) :: var
                end subroutine omp_set_nest_lock

                subroutine omp_set_nested ( enable_expr )
                        import :: omp_logical_kind
                        logical ( kind=omp_logical_kind ), intent(in) :: enable_expr
                end subroutine omp_set_nested

                subroutine omp_set_num_threads ( number_of_threads_expr )
                        import :: omp_integer_kind
                        integer ( kind=omp_integer_kind ), intent(in) :: number_of_threads_expr
                end subroutine omp_set_num_threads

                function omp_test_lock ( var )
                        import :: omp_logical_kind, omp_lock_kind
                        logical ( kind=omp_logical_kind ) :: omp_test_lock
                        integer ( kind=omp_lock_kind ), intent(inout) :: var
                end function omp_test_lock

                function omp_test_nest_lock ( var )
                        import :: omp_integer_kind, omp_nest_lock_kind
                        integer ( kind=omp_integer_kind ) :: omp_test_nest_lock
                        integer ( kind=omp_nest_lock_kind ), intent(inout) :: var
                end function omp_test_nest_lock

                subroutine omp_unset_lock ( var )
                        import :: omp_lock_kind
                        integer ( kind=omp_lock_kind ), intent(inout) :: var
                end subroutine omp_unset_lock

                subroutine omp_unset_nest_lock ( var )
                        import :: omp_nest_lock_kind
                        integer ( kind=omp_nest_lock_kind ), intent(inout) :: var
                end subroutine omp_unset_nest_lock
        end interface
        
end module omp_lib
