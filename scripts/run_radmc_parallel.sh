#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=15gb
#SBATCH --time=15:00:00
#SBATCH --account=AGWalch
#SBATCH --partition=smp
###SBATCH --mail-user=nuernb@ph1.uni-koeln.de

#SBATCH --job-name=radmc_image_parallel
#SBATCH --output=output-%j.out
#SBATCH --error=errors-%j.out

module load szlib/2.1
module load intel
module load gsl/1.13
module load python/2.5.7-V2

