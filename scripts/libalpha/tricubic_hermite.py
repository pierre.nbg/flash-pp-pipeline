import numpy as np
import sys

__author__ = "Michael Weis"
__version__ = "0.0.1.4"

################################################################################
##### CLASS: tricubic interpolation ############################################
################################################################################
# This is tensor based full tricubic interpolation implemented in Hermite form,
# using the constraint choices of Lekien, Marsden (2005).
# Coefficients are precalculated on first subcube hit.
# Caution: Monotony enforcement not completely working yet.

# Prepare coefficients for trilinear interpolation
binmesh_y, binmesh_z, binmesh_x = np.meshgrid((0,1),(0,1),(0,1))
binmesh_xf = binmesh_x.flatten()
binmesh_yf = binmesh_y.flatten()
binmesh_zf = binmesh_z.flatten()

# Prepare coefficients for tricubic interpolation
ijk = (0,1,2,3)
mesh_j, mesh_k, mesh_i = np.meshgrid(ijk, ijk, ijk)
tri_i = mesh_i.flatten()
tri_j = mesh_j.flatten()
tri_k = mesh_k.flatten()
tri_x = np.expand_dims(binmesh_xf, -1)
tri_y = np.expand_dims(binmesh_yf, -1)
tri_z = np.expand_dims(binmesh_zf, -1)

# Construct tricubic interpolator core matrix (see F. Lekien & J. Marsden 2005)
def d01(x,p,d=0):
    """ Evaluate 0. or 1. derivative d of x to the positive power of p """
    return np.logical_not(p<0)* p**d * x**np.clip(p-d,0,None)
#
tri_bf     = d01(tri_x, tri_i)    * d01(tri_y, tri_j)    * d01(tri_z, tri_k)
tri_bf_x   = d01(tri_x, tri_i, 1) * d01(tri_y, tri_j)    * d01(tri_z, tri_k)
tri_bf_y   = d01(tri_x, tri_i)    * d01(tri_y, tri_j, 1) * d01(tri_z, tri_k)
tri_bf_z   = d01(tri_x, tri_i)    * d01(tri_y, tri_j)    * d01(tri_z, tri_k, 1 )
tri_bf_xy  = d01(tri_x, tri_i, 1) * d01(tri_y, tri_j, 1) * d01(tri_z, tri_k)
tri_bf_yz  = d01(tri_x, tri_i)    * d01(tri_y, tri_j, 1) * d01(tri_z, tri_k, 1)
tri_bf_zx  = d01(tri_x, tri_i, 1) * d01(tri_y, tri_j)    * d01(tri_z, tri_k, 1)
tri_bf_xyz = d01(tri_x, tri_i, 1) * d01(tri_y, tri_j, 1) * d01(tri_z, tri_k, 1)
#
tri_B = np.concatenate((tri_bf, tri_bf_x, tri_bf_y, tri_bf_z,
                        tri_bf_xy, tri_bf_yz, tri_bf_zx, tri_bf_xyz))
tri_M = np.linalg.inv(tri_B)
tri_M[np.round(tri_M, 10)==0.] = 0.


class tricubic(object):
    def __enter__(self):
        return self
    def __exit__(self, *exc):
        return False
    def __del__(self):
        pass
    def __init__(self, gbld, verbose=False, ng=2, monotone=True):
        self.blocks, self.nz, self.ny, self.nx = (gbld[:,ng:-ng,ng:-ng,ng:-ng]).shape
        self.ng = ng
        # Fit coefficient matrices for tricubic interpolation
        # Modify for monotone (cubic) interpolation (see Wikipedia) 
        if verbose:
            print 'Estimating tricubic slope coefficients...'
            sys.stdout.flush()
        # ----- Get 1st. order slopes ----->
        gbld_secant_x = gbld[:,:,:,1:] -gbld[:,:,:,:-1]
        gbld_secant_y = gbld[:,:,1:,:] -gbld[:,:,:-1,:]
        gbld_secant_z = gbld[:,1:,:,:] -gbld[:,:-1,:,:]
        gbld_dx = .5* (gbld_secant_x[:,:,:,:-1] +gbld_secant_x[:,:,:,1:])
        gbld_dy = .5* (gbld_secant_y[:,:,:-1,:] +gbld_secant_y[:,:,1:,:])
        gbld_dz = .5* (gbld_secant_z[:,:-1,:,:] +gbld_secant_z[:,1:,:,:])
        #
        if monotone:
            # constrain tangent slope to preserve monotony -> prevent overshoot,
            # see Fritsch, Carlson 1978
            with np.errstate(divide='ignore', invalid='ignore'):
                ## left tangent / secant:
                alpha_x = gbld_dx[:,:,:,:-1] / gbld_secant_x[:,:,:,1:-1]
                alpha_y = gbld_dy[:,:,:-1,:] / gbld_secant_y[:,:,1:-1,:]
                alpha_z = gbld_dz[:,:-1,:,:] / gbld_secant_z[:,1:-1,:,:]
                ## right tangent / secant:
                beta_x = gbld_dx[:,:,:,1:] / gbld_secant_x[:,:,:,1:-1]
                beta_y = gbld_dy[:,:,1:,:] / gbld_secant_y[:,:,1:-1,:]
                beta_z = gbld_dz[:,1:,:,:] / gbld_secant_z[:,1:-1,:,:]
                ## constrain radius of vector (alpha_i, beta_i) to radius 3;
                ## calculate a coefficient tau for each secant giving a lowering
                ## factor for the left and right tangent of those secants:
                tau_x = 3./np.sqrt(alpha_x**2+beta_x**2)
                tau_y = 3./np.sqrt(alpha_y**2+beta_y**2)
                tau_z = 3./np.sqrt(alpha_z**2+beta_z**2)
            ## prepare neutral tangent modification factors:
            tangent_f_x = np.ones_like(gbld_dx)
            tangent_f_y = np.ones_like(gbld_dy)
            tangent_f_z = np.ones_like(gbld_dz)
            ## if a secants tau<1 then stipulate to apply tau to its left tangent;
            ## if alpha is negative, set tangent slope naught:
            with np.errstate(divide='ignore', invalid='ignore'):
                ax = np.nan_to_num(np.where(beta_x<0., 1./alpha_x, tau_x))
                ay = np.nan_to_num(np.where(beta_y<0., 1./alpha_y, tau_y))
                az = np.nan_to_num(np.where(beta_z<0., 1./alpha_z, tau_z))
                np.clip(tangent_f_x[:,:,:,:-1], 0., ax*(alpha_x>=0.),
                        tangent_f_x[:,:,:,:-1])
                np.clip(tangent_f_y[:,:,:-1,:], 0., ay*(alpha_y>=0.),
                        tangent_f_y[:,:,:-1,:])
                np.clip(tangent_f_z[:,:-1,:,:], 0., az*(alpha_z>=0.),
                        tangent_f_z[:,:-1,:,:])
            ## if a secants tau<1 then stipulate to apply tau to its right tangent:
            ## if beta is negative, set tangent slope naught:
            with np.errstate(divide='ignore', invalid='ignore'):
                bx = np.nan_to_num(np.where(alpha_x<0., 1./beta_x, tau_x))
                by = np.nan_to_num(np.where(alpha_y<0., 1./beta_y, tau_y))
                bz = np.nan_to_num(np.where(alpha_z<0., 1./beta_z, tau_z))
                np.clip(tangent_f_x[:,:,:,1:], 0., bx*(beta_x>=0.),
                        tangent_f_x[:,:,:,1:])
                np.clip(tangent_f_y[:,:,1:,:], 0., by*(beta_y>=0.),
                        tangent_f_y[:,:,1:,:])
                np.clip(tangent_f_z[:,1:,:,:], 0., bz*(beta_z>=0.),
                        tangent_f_z[:,1:,:,:])
            ## Apply tangent modification factors:
            gbld_dx *= tangent_f_x
            gbld_dy *= tangent_f_y
            gbld_dz *= tangent_f_z        
        # ----- Get 2nd. order slopes ----->
        gbld_dz_secant_x = gbld_dz[:,:,:,1:] -gbld_dz[:,:,:,:-1]
        gbld_dx_secant_y = gbld_dx[:,:,1:,:] -gbld_dx[:,:,:-1,:]
        gbld_dy_secant_z = gbld_dy[:,1:,:,:] -gbld_dy[:,:-1,:,:]
        gbld_dz_dx    = .5* (gbld_dz_secant_x[:,:,:,1:] +gbld_dz_secant_x[:,:,:,:-1])
        gbld_dx_dy    = .5* (gbld_dx_secant_y[:,:,1:,:] +gbld_dx_secant_y[:,:,:-1,:])
        gbld_dy_dz    = .5* (gbld_dy_secant_z[:,1:,:,:] +gbld_dy_secant_z[:,:-1,:,:])        
        # ----- Get 3rd. order slope ----->
        gbld_dx_dy_secant_z = gbld_dx_dy[:,1:,:,:]-gbld_dx_dy[:,:-1,:,:]
        gbld_dx_dy_dz = .5* (gbld_dx_dy_secant_z[:,1:,:,:] +gbld_dx_dy_secant_z[:,:-1,:,:])
        # ----- Store ----->
        self.p0 = gbld[:,1:-1,1:-1,1:-1]
        self.p1 = gbld_dx[:,1:-1,1:-1,:]
        self.p2 = gbld_dy[:,1:-1,:,1:-1]
        self.p3 = gbld_dz[:,:,1:-1,1:-1]
        self.p4 = gbld_dx_dy[:,1:-1,:,:]
        self.p5 = gbld_dy_dz[:,:,:,1:-1]
        self.p6 = gbld_dz_dx[:,:,1:-1,:]
        self.p7 = gbld_dx_dy_dz
        self.bl_tri_a = np.full(
            (len(gbld), self.nz+2*ng-3, self.ny+2*ng-3, self.nx+2*ng-3, 64), np.nan)

    def __fit_coeff(self, verbose=False, location=None):
        ng = self.ng
        # ----- Fit coefficient matrix ----->        
        if verbose:
            print 'Fitting tricubic coefficient matrix...'
            sys.stdout.flush()
        if location is None:
            irx = xrange(0, self.nx+2*ng-3)
            iry = xrange(0, self.ny+2*ng-3)
            irz = xrange(0, self.nz+2*ng-3)
            ibl = xrange(len(self.p0))
            iz0, bl, iy0, ix0 = np.meshgrid(irz, ibl, iry, irx)
        else:
            bl, ix0, iy0, iz0 = location
        #
        bl_tri_a = 0.
        for node in xrange(8):
            ix = ix0 + binmesh_xf[node]
            iy = iy0 + binmesh_yf[node]
            iz = iz0 + binmesh_zf[node]
            bl_tri_a += tri_M[:,node] * self.p0[bl,iz,iy,ix,None]
            bl_tri_a += tri_M[:,node+8] * self.p1[bl,iz,iy,ix,None]
            bl_tri_a += tri_M[:,node+16] * self.p2[bl,iz,iy,ix,None]
            bl_tri_a += tri_M[:,node+24] * self.p3[bl,iz,iy,ix,None]
            bl_tri_a += tri_M[:,node+32] * self.p4[bl,iz,iy,ix,None]
            bl_tri_a += tri_M[:,node+40] * self.p5[bl,iz,iy,ix,None]
            bl_tri_a += tri_M[:,node+48] * self.p6[bl,iz,iy,ix,None]
            bl_tri_a += tri_M[:,node+56] * self.p7[bl,iz,iy,ix,None]
            if verbose:
                print '*',
                sys.stdout.flush()
        if verbose:
            print '!'
            sys.stdout.flush()
        return bl_tri_a
    
    def __prepare_eval(self, block, icx, icy, icz, verbose=True):
        J = np.where(np.isnan(self.bl_tri_a[block,icz,icy,icx,0]))
        blk = block.astype(np.int32)
        # Eliminate location duplicates
        loclist = zip(blk[J],icx[J],icy[J],icz[J])
        if verbose:
            print len(block), 'cubes,', len(blk[J]), 'unknown',
        if len(loclist) == 0:
            print '-> abort'
            sys.stdout.flush()
            return
        blk_, icx_, icy_, icz_ = np.array(list(set(loclist))).T
        loc = (blk_, icx_, icy_, icz_)
        if verbose:
            print '->', len(blk_), 'unique'
            sys.stdout.flush()
        self.bl_tri_a[blk_, icz_, icy_, icx_] = self.__fit_coeff(
            verbose=False, location=loc)
    
    def eval(self, block, (fcz, fcy, fcx)):
        ng = self.ng
        # Separate float cell indicies into floor integer ic and offset dc
        ## Temporarily shift values up to evade rounding dirswitch at 0.
        icx = (fcx+ng).astype(np.int32) -ng
        icy = (fcy+ng).astype(np.int32) -ng
        icz = (fcz+ng).astype(np.int32) -ng
        # Calculate cell indicies float offset
        dcx = np.expand_dims(fcx-icx, -1)
        dcy = np.expand_dims(fcy-icy, -1)
        dcz = np.expand_dims(fcz-icz, -1)
        #
        self.__prepare_eval(block, icx+ng-1, icy+ng-1, icz+ng-1)
        #
        A = self.bl_tri_a[block, icz+ng-1, icy+ng-1, icx+ng-1]
        xyzp = dcx**tri_i * dcy**tri_j * dcz**tri_k
        return np.sum(A*xyzp, axis=-1)
