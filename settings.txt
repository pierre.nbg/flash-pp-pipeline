# Flags
# Coords has to have shape [dx, dy, dz, x1, x2, y1, y2, z1, z2] for dtype 'octtree' and
# shape [xmin, ymin, zmin, xmax, ymax, zmax] for dtype 'flash'.
# The flag reduc specifies for dtype 'flash' if everything outside of the given coords is
# set to 1e-60 (besides temperature and velocities).
FLAGS
mol_type ['co']
wf [0]
threshold [20000]
freezeout [1]
crir [3.e-17]
dust_to_gas [0.01]
coords []
chemnet 'NL97'
dtype 'flash'
mol_mass [2.3]
spin_temp [0]
mode 'parallel'
dust_temp '0'
outputname [3]
reduc 'False'
ftype 'binary'
FLAGSEND


# Gives settings for wavelength range
# nlam will always set to an odd number to ensure that the central wavelength has its own bin.
WAVELENGTHSETTINGS
lam_0 [2600.758]
nlam [400]
widthkms [20]
WAVELENGTHSETTINGSEND


# Give all folders in data/ which contain files to be run with radmc. Order not important.
STRUCTURE
no_SN
SN
STRUCTUREEND


# Specify for which directions you want to make the observations. These names will also be used
# for folder creation, so you should replace spaces with underscores. 
ORIENTATIONS
incl_0_phi_0
incl_90_phi_0
incl_90_phi_-90
ORIENTATIONSEND


# Gives radmc3d call options. Order is IMPORTANT.
# The orientation from above will be added without underscores automatically.
# Keep in mind that the value for a parsec in RADMC is 3.08572e18 cm.
RADMC3DCALL
image
loadlambda
npix 1024
sizepc 125
secondorder
doppcatch
RADMC3DCALLEND


# Gives settings for radmc3d.inp file
RADMC3DSET
lines_mode = 3
lines_nonlte_maxiter = 100
lines_nonlte_convcrit = 0.001
tgas_eq_tdust = 0
scattering_mode_max = 0
incl_dust = 0
incl_lines = 1
catch_doppler_resolution = 0.05
lines_slowlvg_as_alternative = 1
RADMC3DSETEND


# Give settings for lines.inp file. Order is IMPORTANT. 
# Has to have header as stated in RADMC manual.
LINESSETTINGS
2
1
co    leiden    0    0    1
h2
LINESSETTINGSEND


# There are two restart options if you want to change some settings but don't want to modify the
# data.
# If you add 'radmc_parallel' with the python call you can rewrite the radmc_parallel.sh files.
# If you add 'add_input' with the python call you can rewrite the input files 
# radmc3d.inp, lines.inp, wavelength_micron.inp, molecule_%s.inp and camera_wavelength_micron.inp.
